from rest_framework import serializers

from users.models import BankAccount, Operation
from users.services.services import transfer


class BankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        fields = ['id', 'user', 'balance']


class OperationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Operation
        fields = ['amount', 'message', 'sender', 'receiver', 'date']


class TransferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Operation
        fields = ['amount', 'message', 'sender', 'receiver']

    def validate(self, data):
        if data['amount'] > data['sender'].balance:
            raise serializers.ValidationError("Sender don't have enough money.")
        if data['sender'] == data['receiver']:
            raise serializers.ValidationError("Sender and receiver must not be equal.")
        return data

    def save(self, **kwargs):
        instance = transfer(self.validated_data['sender'], self.validated_data['receiver'],
                            self.validated_data['amount'],
                            self.validated_data['message'])
        return instance
